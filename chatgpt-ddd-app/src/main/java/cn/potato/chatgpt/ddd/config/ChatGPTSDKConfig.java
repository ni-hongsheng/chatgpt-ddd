package cn.potato.chatgpt.ddd.config;
import cn.potato.chatgpt.interceptor.Authentication;
import cn.potato.chatgpt.session.OpenAiSession;
import cn.potato.chatgpt.session.OpenAiSessionFactory;
import cn.potato.chatgpt.session.defaults.DefaultOpenAiSessionFactory;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 *  OpenAiSession 工厂配置开启
 *
 *  引入SDK 后，将openaisession 设置成bean 进行初始化
 */
@Configuration
@EnableConfigurationProperties(ChatGPTSDKConfigProperties.class)
public class ChatGPTSDKConfig {

    @Bean
    public OpenAiSession openAiSession(ChatGPTSDKConfigProperties properties) throws IOException {
        // 1. 配置文件
        // jdk里面的configuration 与 springboot配置里的重复了
        cn.potato.chatgpt.session.Configuration configuration = new cn.potato.chatgpt.session.Configuration();
        configuration.setApiHost(properties.getApiHost());
        configuration.setAuthToken(properties.getAuthToken());
        configuration.setApiKey(properties.getApiKey());

        // 鉴权
//        Authentication authentication = new Authentication(configuration);
//        authentication.authentication();

        // 2. 会话工厂
        OpenAiSessionFactory factory = new DefaultOpenAiSessionFactory(configuration);

        // 3. 开启会话
        return factory.openSession();
    }

}

