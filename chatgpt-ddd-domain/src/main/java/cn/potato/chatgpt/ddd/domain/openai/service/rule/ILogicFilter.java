package cn.potato.chatgpt.ddd.domain.openai.service.rule;

import cn.potato.chatgpt.ddd.domain.openai.model.aggregates.ChatProcessAggregate;
import cn.potato.chatgpt.ddd.domain.openai.model.entity.RuleLogicEntity;

public interface ILogicFilter {

    RuleLogicEntity<ChatProcessAggregate> filter(ChatProcessAggregate chatProcess) throws Exception;

}

