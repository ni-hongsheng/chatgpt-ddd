package cn.potato.chatgpt.domain.security.domain.vo;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * 这段代码定义了一个实现了AuthenticationToken接口的JwtToken类，用于在Shiro框架中表示包含JWT（JSON Web Token）的认证凭据。
 *
 * 其中jwt字段表示JWT字符串类型的凭据。getPrincipal()和getCredentials()方法则是AuthenticationToken接口中的两个方法，
 * 用于返回认证主体和凭据。在这个实现中，认证主体和凭据都是同一个JWT字符串。具体来说：
 *
 * getPrincipal()方法返回JWT字符串，它在Shiro框架中通常被当做用户身份表示。
 * 可以通过调用SecurityUtils.getSubject().getPrincipal()方法获取当前已认证用户的身份。
 *
 * getCredentials()方法也返回JWT字符串，它在Shiro框架中通常被当做用户凭据，用于在JWT验证过程中校验JWT的真实性。
 *
 * 当需要在Shiro中使用JWT进行身份验证时，可以通过创建并传入JwtToken对象来表示用户身份和凭据。
 * Shiro框架会将其传递给对应的Realm实现来完成验证过程。
 */
public class JwtToken implements AuthenticationToken {

    private String jwt;

    public JwtToken(String jwt){
        this.jwt = jwt;
    }

    @Override
    public Object getPrincipal() {
        return jwt;
    }

    @Override
    public Object getCredentials() {
        return jwt;
    }
}
