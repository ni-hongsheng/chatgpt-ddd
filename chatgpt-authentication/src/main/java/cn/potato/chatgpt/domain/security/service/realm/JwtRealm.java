package cn.potato.chatgpt.domain.security.service.realm;


import cn.potato.chatgpt.domain.security.domain.vo.JwtToken;
import cn.potato.chatgpt.domain.security.service.JwtUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 这段代码定义了一个继承自AuthorizingRealm的JwtRealm类，用于在Shiro框架中处理JWT（JSON Web Token）的认证和授权。
 *
 * 在supports(AuthenticationToken token)方法中，JwtRealm指定了它支持处理JwtToken类型的认证凭据。
 * 这意味着当Shiro接收到JwtToken类型的凭据时，会将其委托给JwtRealm来完成认证过程。
 *
 * 在doGetAuthenticationInfo(AuthenticationToken token)方法中，JwtRealm实现了JWT的认证逻辑。
 * 首先，从传入的token中获取JWT字符串，并进行一些非空检查。然后，通过jwtUtil.isVerify(jwt)方法判断JWT是否有效。
 * 如果无效，抛出UnknownAccountException异常表示未知的账户。
 *
 * 接下来，可以从JWT中提取用户名等信息，并进行相关处理。在示例中，通过jwtUtil.decode(jwt).get("username")获取了用户名信息，
 * 并使用日志记录了鉴权用户的用户名。
 *
 * 最后，通过创建一个SimpleAuthenticationInfo对象返回认证信息。在这个简单实现中，认证主体、凭据和realm名称都设置为JWT字符串。
 */
public class JwtRealm extends AuthorizingRealm {
    private Logger logger = LoggerFactory.getLogger(JwtRealm.class);

    private static JwtUtil jwtUtil = new JwtUtil();

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // 暂时不需要实现
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String jwt = (String) token.getPrincipal();
        if (jwt == null) {
            throw new NullPointerException("jwtToken 不允许为空");
        }
        // 判断
        if (!jwtUtil.isVerify(jwt)) {
            throw new UnknownAccountException();
        }
        // 可以获取username信息，并做一些处理
        String username = (String) jwtUtil.decode(jwt).get("username");
        logger.info("鉴权用户 username：{}", username);
        return new SimpleAuthenticationInfo(jwt, jwt, "JwtRealm");
    }

}
